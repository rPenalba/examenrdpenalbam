
package com.is2t1.app.cotroller;

import com.is2t1.app.views.ContactFrame;
import com.is2t1.app.views.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainController implements ActionListener{
    MainFrame mf;
      
    public MainController(MainFrame mf) {
        this.mf = mf;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()) {
            case "NewContact" :
                showNewFrame();
                break;
            case "Open":
                break;
            
        }
    }
    
    public void showNewFrame() {
        ContactFrame cF = new ContactFrame();
        mf.showChild(cF);
        
    }
    
}
